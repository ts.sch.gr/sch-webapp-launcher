#!/bin/sh
# Open a URL in Palemoon or other browser that supports Adobe Flash
# Copyright (C) 2012-2022 Alkis Georgopoulos <github.com/alkisg>
# SPDX-License-Identifier: GPL-3.0-or-later

die() {
	echo "$@" >&2
	exit 1
}

# Copy stock flash settings with no acceleration
flash_settings() {
	local sfile

	if [ -z "$HOME" ] || [ ! -d "$HOME" ]; then
		echo "\$HOME is not defined, can't set flash settings"
		return 1
	fi
	sfile="$HOME/.macromedia/Flash_Player/macromedia.com/support/flashplayer/sys/settings.sol"
	grep -qs jammy /etc/os-release || return 0
	grep -qs "safefullscreen$(printf "\001\001")" "$sfile" && return 0
	mkdir -pm 700 "$HOME/.marcomedia"
	mkdir -p "${sfile%/*}"
	cp /usr/share/sch-webapp-launcher/settings.sol "$sfile"
}

process_urls() {
	local url uri

	for url; do
		case "$url" in
		"")
			continue
			;;
		http:* | https:* | file:*) ;;

		*)
			if [ -e "/usr/share/$url/webapp/webapp.ini" ]; then
				# webapp.ini might have DOS line breaks.
				uri=$(sed -n "s/uri=//p" "/usr/share/$url/webapp/webapp.ini" | tr -d '\r')
				test -n "$uri" && url="$uri"
			fi
			;;
		esac
		printf "%s\n" "$url"
	done
}

main() {
	local urls browser

	urls=$(process_urls "$@")
	test -n "$urls" || die "Usage: sch-webapp-launcher <URLs>
It supports the usual http, https and file protocols,
but it also searches for webapps in /usr/share/appname/webapp"

	# Convert $urls to $@
	_OLDIFS="${IFS-not set}"
	IFS="
"
	set -- $urls
	test "$_OLDIFS" = "not set" && unset IFS || IFS="$_OLDIFS"

	flash_settings

	# Detect browser
	for browser in sch-webapp-browser palemoon midori firefox chromium-browser google-chrome x-www-browser; do
		command -v "$browser" >/dev/null && exec "$browser" "$@"
	done
	exec xdg-open "$@"
}

main "$@"
